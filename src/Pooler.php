<?php

namespace Sanpi\PommProject\TemplateQuery;

use \PommProject\Foundation\Client\ClientPooler;

class Pooler extends ClientPooler
{
    private $template_directory;

    public function __construct($template_directory)
    {
        $this->template_directory = $template_directory;
    }

    public function getPoolerType()
    {
        return 'template_query';
    }

    protected function createClient($identifier)
    {
        return new Client("{$this->template_directory}/$identifier.sql");
    }
}
