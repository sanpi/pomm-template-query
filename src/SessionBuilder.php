<?php

namespace Sanpi\PommProject\TemplateQuery;

use \PommProject\Foundation\Session\Session;
use \PommProject\Foundation\Session\Connection;
use \PommProject\Foundation\Client\ClientHolder;
use \PommProject\Foundation\SessionBuilder as PommSessionBuilder;

class SessionBuilder extends PommSessionBuilder
{
    protected function postConfigure(Session $session)
    {
        parent::postConfigure($session);

        $template_directory = $this->configuration->getParameter('dir:template');
        $pooler = new Pooler($template_directory);

        $session->registerClientPooler($pooler);

        return $this;
    }

    protected function createSession(Connection $connection, ClientHolder $client_holder, $stamp)
    {
        $this->configuration->setDefaultValue('dir:template', 'src/sql');

        return parent::createSession($connection, $client_holder, $stamp);
    }
}
