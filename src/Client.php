<?php

namespace Sanpi\PommProject\TemplateQuery;

use \PommProject\Foundation\Exception\PommException;
use \PommProject\Foundation\PreparedQuery\PreparedQuery;

class Client extends PreparedQuery
{
    private $parameters = [];

    public function __construct($filename)
    {
        if (is_file($filename)) {
            $sql = file_get_contents($filename);
            parent::__construct($sql);
        }
        else {
            throw new PommException("Template file '$filename' doesn't exist");
        }
    }

    public function getClientType()
    {
        return 'template_query';
    }

    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function execute(array $values = [])
    {
        $sql = $this->sql;

        $this->sql = strtr($sql, $this->parameters);
        $resource = parent::execute($values);
        $this->sql = $sql;

        return $resource;
    }
}
