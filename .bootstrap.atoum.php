<?php

require __DIR__ . '/vendor/autoload.php';
$config = __DIR__ . '/tests/config.php';

if (file_exists($config)) {
    require $config;
}
else {
    require __DIR__ . '/tests/config.travis.php';
}
