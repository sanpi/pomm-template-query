<?php

namespace Test\Unit\Sanpi\PommProject\TemplateQuery;

use \PommProject\Foundation\Converter\Type\Circle;

class Client extends \atoum
{
    use Testable;

    public function testTemplate()
    {
        $pomm = $this->getPomm();

        $pomm['pomm_db1']->getTemplateQuery('long_query')
            ->execute([
                2,
                ['pika, chu', 'three'],
                new \DateTime('2000-01-01'),
                new Circle('<(1.5,1.5), 0.3>')
            ]);
    }

    public function testMissingTemplate()
    {
        $pomm = $this->getPomm();

        $this->exception(function () use($pomm) {
            $pomm['pomm_db1']->getTemplateQuery('wrong_filename')
                ->execute();
        });
    }

    public function testTemplateWithPlaceholder()
    {
        $pomm = $this->getPomm();

        $pomm['pomm_db1']->getTemplateQuery('long_query_parameter')
            ->setParameters([
                ':projection' => 'p.id, p.pika, p.a_timestamp, p.a_point',
            ])
            ->execute([
                2,
                ['pika, chu', 'three'],
                new \DateTime('2000-01-01'),
                new Circle('<(1.5,1.5), 0.3>')
            ]);
    }
}
