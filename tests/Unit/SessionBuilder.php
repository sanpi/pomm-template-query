<?php

namespace Test\Unit\Sanpi\PommProject\TemplateQuery;

class SessionBuilder extends \atoum
{
    use Testable;

    public function testRegistration()
    {
        $builder = $this->getPomm()
            ->getBuilder('pomm_db1');

        $this->object($builder)
            ->isInstanceOf($this->getTestedClassName());
    }
}
