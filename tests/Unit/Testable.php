<?php

namespace Test\Unit\Sanpi\PommProject\TemplateQuery;

use \PommProject\Foundation\Pomm;

trait Testable
{
    public function getPomm()
    {
        return new Pomm([
            'pomm_db1' => [
                'dsn' => $GLOBALS['pomm_db1']['dsn'],
                'class:session_builder' => '\Sanpi\PommProject\TemplateQuery\SessionBuilder',
                'dir:template' => __DIR__ . '/../sql/',
            ],
        ]);
    }
}
