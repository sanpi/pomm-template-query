select :projection
from (values
    (1, 'one', '1999-08-08'::timestamp, ARRAY[point(1.3, 1.6)]),
    (2, 'two', '2000-09-07'::timestamp, ARRAY[point(1.5, 1.5)]),
    (3, 'three', '2001-10-25 15:43'::timestamp, ARRAY[point(1.6, 1.4)]),
    (4, 'four', '2002-01-01 01:10'::timestamp, ARRAY[point(1.8, 2.3)])
) p (id, pika, a_timestamp, a_point)
where (p.id >= $* or p.pika = ANY($*::text[]))
    and p.a_timestamp > $*::timestamp
    and $*::pg_catalog."circle" @> ANY (p.a_point);
