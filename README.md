# Pomm template query

Because one programming language per file is enough.

## Installation

```
composer require sanpi/pomm-template-query
```

## Configuration

Use the new session builder and specify the template directory in pomm
configuration:

```php
new Pomm([
    'pomm_db1' => [
        'dsn' => …,
        'class:session_builder' => '\Sanpi\PommProject\TemplateQuery\SessionBuilder',
        'dir:template' => __DIR__ . '/sql/',
    ],
]);
```

## Use

Instead of loading a query, you specify an alias:

```php
$pomm['pomm_db1']->getTemplateQuery('long_query')
    ->execute([2]);
```

Now pomm searches a file in `dir:template` directory named `long_query.sql`.

You can make some replacement before executing the query. This is usefull with
the model manager:

```php
$pomm['pomm_db1']->getTemplateQuery('long_query_parameter')
    ->setParameters([
        ':projection' => $this->createProjection(),
    ])
    ->execute([2]);
```
